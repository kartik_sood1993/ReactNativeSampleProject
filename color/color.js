export default {
  white: "#ffffff",
  lightYellow: "#fffccc",
  accent: "#c717fc",
  headerColor: "#7cd212"
};
