import React, { useState } from "react";
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Keyboard,
} from "react-native";

import ColorInput from "../color/color";

const GoalInput = (props) => {
  const [enteredGoal, setEnteredGoal] = useState("");

  const goalInputHandler = (enteredText) => {
    setEnteredGoal(enteredText);
  };

  const addGoalHandler = () => {
    props.onAddGoal(enteredGoal);
    setEnteredGoal("");
    Keyboard.dismiss();
  };

  return (
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Add New Goal"
          style={styles.textInput}
          onChangeText={goalInputHandler}
          value={enteredGoal}
        ></TextInput>
        <View style={styles.buttonContainer}>
          <Button title="ADD" onPress={addGoalHandler} />
        </View>
      </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
  },
  textInput: {
    width: "80%",
    borderColor: ColorInput.borderColor,
    borderWidth: 1,
    padding: 10,
  },
  buttonContainer: {
    marginLeft: 20,
    justifyContent: "center",
  },
});

export default GoalInput;
