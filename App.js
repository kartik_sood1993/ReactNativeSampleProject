import React, { useState } from "react";
import { View, StyleSheet, FlatList } from "react-native";

import GoalInput from "./component/GoalInput";
import GoalItem from "./component/GoalItem";
import DefaultStyles from "./styles/Default-styles";
import color from "./color/color";

export default function App() {
  const [courseGoals, setCourseGoals] = useState([]);

  const addGoalHandler = (goalTitle) => {
    setCourseGoals((currentGoal) => [
      ...currentGoal,
      { key: Math.random().toString(), value: goalTitle },
    ]);
  };

  return (
    <View style={DefaultStyles.conatiner}>
      <GoalInput onAddGoal={addGoalHandler} />

      <FlatList
        keyExtractor={(item, index) => item.key}
        data={courseGoals}
        renderItem={(itemData) => (
          <GoalItem
            id={itemData.item.key}
            onDelete={() => console.log("delete dunction call")}
            title={itemData.item.value}
          />
        )}
      ></FlatList>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    marginTop: 20,
  },
});
